package datastructure;

import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {

    // Antall kolonner i grid
    private int cols;

    // Antall rader i grid
    private int rows;

    // Datastrukturen som skal brukes i game of life
    private ArrayList<CellState> CellList;


    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;

        int numCells = rows * columns;
        this.CellList = new ArrayList<CellState>();

        // "Forteller" cellene om de skal starte død/i live.
        for(int i = 0; i < numCells; i++) {
            CellList.add(initialState);
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    private int getIndex(int row, int col) {

		// Formel for (x, y), men lar den stå her
		// ( (row - 1) * this.cols + col ) - 1;

        // Sjekker om index er med i spillet eller utenfor / under 0.
		if (row >= this.numRows() || col >= this.numColumns()) {
			throw new IndexOutOfBoundsException("Too high index");
		}
		else if (row < 0 || col < 0){
			throw new IndexOutOfBoundsException("Too low index");
		}

		return row * this.cols + col;
	}

    @Override
    public void set(int row, int column, CellState element) {
        // Henter index fra getIndex metode
        int index = getIndex(row, column);

        // Setter/bytter elementet i den spesifike indexen gjennom .set metoden fra ArrayList.
        CellList.set(index, element);
    }

    @Override
    public CellState get(int row, int column) {
        int index = getIndex(row, column);

        // Hender CellStaten til indexen gjennom .get metoden fra ArrayList.
        return CellList.get(index);
    }

    @Override
    public IGrid copy() {
        // Lager en cellGrid med rows and columns og alle cellene døde
		CellGrid newGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);

		// Oppdaterer cellene med de states de egentlig skal ha
		for (int row = 0; row < this.rows; row++) {
			for (int col = 0; col < this.cols; col++) {
				// Henter ut staten for row og col i originale cell griden
				CellState value = this.get(row, col);

				// Setter den samme staten i den nye kopierte griden
				newGrid.set(row, col, value);
			}
		}

		return newGrid;
    }
    
}