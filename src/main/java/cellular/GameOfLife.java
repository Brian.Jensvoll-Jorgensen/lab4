package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = this.currentGeneration.copy();
		
		// For loop går gjennom griddet og henter den neste statusen til cellen i loopen.
		for (int row = 0; row < numberOfRows(); row++) {
			for (int col = 0; col < numberOfColumns(); col++) {
				// Henter neste Staten
				CellState celle = getNextCell(row, col);
				// Setter den inn i neste generasjon			
				nextGeneration.set(row, col, celle);
			}
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		Boolean deadState = getCellState(row, col) == CellState.DEAD;
		Boolean aliveState = getCellState(row, col) == CellState.ALIVE;

		// Henter antall naboer som er i live
		int naboer = countNeighbors(row, col, CellState.ALIVE);

		// Regel 1: I live og har mindre enn 2 naboer, celle dør.
		if (aliveState && naboer < 2) {
			return CellState.DEAD;
		}
		
		// Regel 2: I live og har 2/3 naboer, celle fortsetter å leve.
		else if (aliveState && (naboer == 2 || naboer == 3)) {
			return CellState.ALIVE;
		}

		// Regel 3: I live og har mer enn 3 naboer, celle dør.
		else if (aliveState && naboer > 3) {
			return CellState.DEAD;	
		}

		// Regel 4: Død og har 3 naboer, celle lever igjen.
		else if (deadState && naboer == 3){
			return CellState.ALIVE;
		}

		// Ingen regler oppfylt, celle forblir går tilbake til original.
		return getCellState(row, col);
	}

	// Metode for å skjekke om raden/kolonnen er i griddet.
	private boolean validIndex(int row, int col) {
		if (row >= numberOfRows() || col >= numberOfColumns()) {
			return false;
		}
		else if (row < 0 || col < 0){
			return false;
		}
		return true;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		// Starter med 0 naboer
		int antNabo = 0;

		// Kjører 3x3 for-loop for x og y verdier fra -1 -> 1 for dx og dy.
		for (int dx = -1; dx <= 1; dx++){
			for (int dy = -1; dy <= 1; dy++){
				
				// Dekker for den original cellen slik at den ikke blir tatt med.
				if (dx == 0 && dy == 0) {
					continue;
				}

				// Sjekker om index er i grid
				else if (validIndex(row + dx, col + dy)) {
					// Henter CellState til nabo
					CellState nabo = getCellState(row + dx, col + dy);

					// Skjekker om de er i live, om så, legg til en nabo
					if (nabo == state) {
						antNabo++;
					}
				}
			}
		}

		return antNabo;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
