package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
		
		// For loop går gjennom griddet og henter den neste statusen til cellen i loopen.
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				CellState celle = getNextCell(row, col);
				nextGeneration.set(row, col, celle);
			}
		}

        currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        Boolean deadState = getCellState(row, col) == CellState.DEAD;
        Boolean dyingState = getCellState(row, col) == CellState.DYING;
		Boolean aliveState = getCellState(row, col) == CellState.ALIVE;

		// Henter antall naboer som er i live
		int naboer = countNeighbors(row, col, CellState.ALIVE);

		// Regel 1: En levende celle blir døende.
		if (aliveState) {
			return CellState.DYING;
		}
		
		// Regel 2: En døende celle dør..
		else if (dyingState) {
			return CellState.DEAD;
		}

		// Regel 3: En død celle med 2 naboer kommer til live igjen.
		else if (deadState && naboer == 2) {
			return CellState.ALIVE;	
		}

		// Regel 4: Ingen andre regler oppfylt, celle er død.
        else if (deadState) {
			return CellState.DEAD;	
		}

		return getCellState(row, col);
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }


    // Metode for å skjekke om raden/kolonnen er i griddet.
	private boolean validIndex(int row, int col) {
		if (row >= numberOfRows() || col >= numberOfColumns()) {
			return false;
		}
		else if (row < 0 || col < 0){
			return false;
		}
		return true;
	}


    private int countNeighbors(int row, int col, CellState state) {
		// Starter med 0 naboer
		int antNabo = 0;

		// Kjører 3x3 for-loop for x og y verdier fra -1 -> 1 for dx og dy.
		for (int dx = -1; dx <= 1; dx++){
			for (int dy = -1; dy <= 1; dy++){
				
				// Dekker for den original cellen slik at den ikke blir tatt med.
				if (dx == 0 && dy == 0) {
					continue;
				}

				// Sjekker om index er i grid
				else if (validIndex(row + dx, col + dy)) {
					// Henter CellState til nabo
					CellState nabo = getCellState(row + dx, col + dy);

					// Skjekker om de er i live, om så, legg til en nabo
					if (nabo.equals(state)) {
						antNabo++;
					}
				}
			}
		}

		return antNabo;
	}
}
